############################################################################
############################################################################
# THIS IS THE ONLY FILE YOU SHOULD EDIT
#
#
# Agent must always have these five functions:
#     __init__(self)
#     has_finished_episode(self)
#     get_next_action(self, state)
#     set_next_state_and_distance(self, next_state, distance_to_goal)
#     get_greedy_action(self, state)
#
#
# You may add any other functions as you wish
############################################################################
############################################################################
import torch
import numpy as np
import collections


class Agent:

    # Function to initialise the agent
    def __init__(self):
        # Set the episode length
        self.episode_length = 700
        # Reset the total number of steps which the agent has taken
        self.num_steps_taken = 0
        # The state variable stores the latest state of the agent in the environment
        self.state = None
        # The action variable stores the latest action which the agent has applied to the environment
        self.action = None
        self.delta = 0.001
        self.epsilon = 1.0
        self.replay = ReplayBuffer()
        self.dqn = DQN()
        self.distance_to_goal = None
        self.episode = 0


    # Function to check whether the agent has reached the end of an episode
    def has_finished_episode(self):
        #might need to change 0.03
        if self.num_steps_taken % self.episode_length == 0 or self.distance_to_goal < 0.03:
            self.dqn.update_target()
            self.episode += 1
            self.epsilon = 1 - np.exp(-10.0/self.episode)
            if self.epsilon < 0.025:
                self.epsilon = 0
            self.delta = self.epsilon*0.001
            return True
        else:
            return False

    # Function to get the next action, using whatever method you like
    def get_next_action(self, state):
        #decide which action to take

        Q_values = self.dqn.q_network.forward(torch.tensor(state)).tolist()
        greedy_action = Q_values.index(max(Q_values))
        probablity = [self.epsilon/4,self.epsilon/4,self.epsilon/4,self.epsilon/4]
        probablity[greedy_action] = 1 - self.epsilon + self.epsilon/4
        discrete_action = np.random.choice(4,1,replace = False,p = probablity)[0]
        continuous_action = self._discrete_action_to_continuous(discrete_action)
        action = continuous_action

        # Update the number of steps which the agent has taken
        self.num_steps_taken += 1
        # Store the state; this will be used later, when storing the transition
        self.state = state
        # Store the action; this will be used later, when storing the transition
        self.action = discrete_action
        return action

    # Function to set the next state and distance, which resulted from applying action self.action at state self.state
    def set_next_state_and_distance(self, next_state, distance_to_goal):
        self.distance_to_goal = distance_to_goal

        reward = self._compute_reward(distance_to_goal,next_state)

        transition = (self.state, self.action, reward, next_state)

        #training and backpropagation
        self.replay.buffer.append(transition)
        if len(self.replay.buffer) >= 100:
            indices = np.random.choice(range(0,len(self.replay.buffer)),50, replace = False)
            batch = []
            for i in indices:
                batch.append(self.replay.buffer[i])
            loss = self.dqn.train_q_network(batch)
            self.epsilon_decay()

    # Function to convert discrete action (as used by a DQN) to a continuous action (as used by the environment).
    def _discrete_action_to_continuous(self, discrete_action):
        if discrete_action == 0:  # Move right
            continuous_action = np.array([0.02, 0], dtype=np.float32)

        if discrete_action == 1:  # Move left
            continuous_action = np.array([-0.02, 0], dtype=np.float32)

        if discrete_action == 2:  # Move up
            continuous_action = np.array([0, 0.02], dtype=np.float32)

        if discrete_action == 3:  # Move down
            continuous_action = np.array([0, -0.02], dtype=np.float32)
            
        return continuous_action

    # Function to get the greedy action for a particular state
    def get_greedy_action(self, state):
        Q_values = self.dqn.q_network.forward(torch.tensor(state)).tolist()
        greedy_action = Q_values.index(max(Q_values))

        continuous_action = self._discrete_action_to_continuous(greedy_action)
        action = continuous_action
        return action

    def _compute_reward(self, distance_to_goal,next_state):
        reward = 1 - distance_to_goal
        if next_state[0] == self.state[0] and next_state[1] == self.state[1]:
            reward = reward*0.7
        if distance_to_goal < 0.1:
            reward = reward*1.5
        return reward

    def epsilon_decay(self):
        self.epsilon -= self.delta
        if self.epsilon < 0:
            self.epsilon = 0

# The DQN class determines how to train the above neural network.
class DQN:

    # The class initialisation function.
    def __init__(self):

        self.q_network = Network(input_dimension=2, output_dimension=4)
        self.target_network = Network(input_dimension=2, output_dimension=4)
        self.optimiser = torch.optim.Adam(self.q_network.parameters(), lr=0.001)
        self.discount = 0.9

    def train_q_network(self, transition):
        self.optimiser.zero_grad()
        loss = self._calculate_loss(transition)
        loss.backward()
        self.optimiser.step()
        return loss.item()

    def _calculate_loss(self, transition):

        states = []
        rewards = []
        actions = []
        next_states = []

        for row in transition:
            actions.append(row[1])
            rewards.append(row[2])
            states.append(row[0])
            next_states.append(row[3])

        states_tensor = torch.tensor(states)
        actions_tensor = torch.tensor(actions).unsqueeze(1)
        rewards_tensor = torch.tensor(rewards).unsqueeze(1)
        next_states_tensor = torch.tensor(next_states)

        #3.1 added
        next_state_network_predictions = self.target_network.forward(next_states_tensor).detach()
        max_next_states_tensor = []
        max_actions = []
        for row in next_state_network_predictions:
            max_actions.append(row.tolist().index(max(row.tolist())))
        max_actions_tensor = torch.tensor(max_actions).unsqueeze(1)
        
        next_predictions = torch.gather(next_state_network_predictions*self.discount,1,max_actions_tensor)

        #stop here

        network_predictions = self.q_network.forward(states_tensor)

        predictions = torch.gather(network_predictions,1,actions_tensor)

        loss = torch.nn.MSELoss()(predictions,next_predictions+rewards_tensor)

        return loss

    def update_target(self):
        weights = self.q_network.state_dict()
        self.target_network.load_state_dict(weights)

# The Network class inherits the torch.nn.Module class, which represents a neural network.
class Network(torch.nn.Module):

    # The class initialisation function. This takes as arguments the dimension of the network's input (i.e. the dimension of the state), and the dimension of the network's output (i.e. the dimension of the action).
    def __init__(self, input_dimension, output_dimension):
        # Call the initialisation function of the parent class.
        super(Network, self).__init__()
        # Define the network layers. This example network has two hidden layers, each with 100 units.
        self.layer_1 = torch.nn.Linear(in_features=input_dimension, out_features=200)
        self.layer_2 = torch.nn.Linear(in_features=200, out_features=200)
        self.layer_3 = torch.nn.Linear(in_features=200, out_features=200)
        self.layer_4 = torch.nn.Linear(in_features=200, out_features=200)
        self.layer_5 = torch.nn.Linear(in_features=200, out_features=100)
        self.output_layer = torch.nn.Linear(in_features=100, out_features=output_dimension)

    def conv2d_size_out(size, kernel_size = 5, stride = 2):
        return (size - (kernel_size - 1) - 1) // stride  + 1
    # Function which sends some input data through the network and returns the network's output. In this example, a ReLU activation function is used for both hidden layers, but the output layer has no activation function (it is just a linear layer).
    def forward(self, input):
        layer_1_output = torch.nn.functional.relu(self.layer_1(input))
        layer_2_output = torch.nn.functional.relu(self.layer_2(layer_1_output))
        layer_3_output = torch.nn.functional.relu(self.layer_3(layer_2_output))
        layer_4_output = torch.nn.functional.relu(self.layer_4(layer_3_output))
        layer_5_output = torch.nn.functional.relu(self.layer_5(layer_4_output))
        output = self.output_layer(layer_5_output)
        return output
 


class ReplayBuffer:
    def __init__(self):
        self.buffer = collections.deque(maxlen = 70000)

