# Import some modules from other libraries
import numpy as np
import torch
import time
from matplotlib import pyplot as plt
import collections

# Import the environment module
from environment import Environment
import cv2


# The Agent class allows the agent to interact with the environment.
class Agent:

    # The class initialisation function.
    def __init__(self, environment, epsilon = 1.0, delta = 0.01):
        # Set the agent's environment.
        self.environment = environment
        # Create the agent's current state
        self.state = None
        # Create the agent's total reward for the current episode.
        self.total_reward = None

        self.epsilon = epsilon
        self.delta = delta
        # Reset the agent.
        self.reset()

    # Function to reset the environment, and set the agent to its initial state. This should be done at the start of every episode.
    def reset(self):
        # Reset the environment for the start of the new episode, and set the agent's state to the initial state as defined by the environment.
        self.state = self.environment.reset()
        # Set the agent's total reward for this episode to zero.
        self.total_reward = 0.0

    # Function to make the agent take one step in the environment.
    def step(self,dqn):
        # Choose an action.
        Q_values = dqn.q_network.forward(torch.tensor(self.state)).tolist()
        greedy_action = Q_values.index(max(Q_values))
        probablity = [self.epsilon/4,self.epsilon/4,self.epsilon/4,self.epsilon/4]
        probablity[greedy_action] = 1 - self.epsilon + self.epsilon/4
        discrete_action = np.random.choice(4,1,replace = False,p = probablity)[0]
        continuous_action = self._discrete_action_to_continuous(discrete_action)
        # Take one step in the environment, using this continuous action, based on the agent's current state. This returns the next state, and the new distance to the goal from this new state. It also draws the environment, if display=True was set when creating the environment object..
        next_state, distance_to_goal = self.environment.step(self.state, continuous_action)
        # Compute the reward for this paction.
        reward = self._compute_reward(distance_to_goal)
        # Create a transition tuple for this step.
        transition = (self.state, discrete_action, reward, next_state)
        # Set the agent's state for the next step, as the next state from this step
        self.state = next_state
        # Update the agent's reward for this episode
        self.total_reward += reward
        # Return the transition
        return transition

    def greedy_step(self,dqn):
        # Choose an action.
        Q_values = dqn.q_network.forward(torch.tensor(self.state)).tolist()
        discrete_action = Q_values.index(max(Q_values))
        # Convert the discrete action into a continuous action.
        continuous_action = self._discrete_action_to_continuous(discrete_action)
        # Take one step in the environment, using this continuous action, based on the agent's current state. This returns the next state, and the new distance to the goal from this new state. It also draws the environment, if display=True was set when creating the environment object..
        next_state, distance_to_goal = self.environment.step(self.state, continuous_action)
        # Compute the reward for this paction.
        reward = self._compute_reward(distance_to_goal)
        # Create a transition tuple for this step.
        transition = (self.state, discrete_action, reward, next_state)
        # Set the agent's state for the next step, as the next state from this step
        self.state = next_state
        # Update the agent's reward for this episode
        self.total_reward += reward
        # Return the transition
        return transition

    def epsilon_decay(self):
        self.epsilon -= self.delta
        if self.epsilon < 0:
            self.epsilon = 0

    # Function for the agent to compute its reward. In this example, the reward is based on the agent's distance to the goal after the agent takes an action.
    def _compute_reward(self, distance_to_goal):
        reward = 1 - distance_to_goal
        return reward

    # Function to convert discrete action (as used by a DQN) to a continuous action (as used by the environment).
    def _discrete_action_to_continuous(self, discrete_action):
        if discrete_action == 0:  # Move right
            continuous_action = np.array([0.1, 0], dtype=np.float32)

        if discrete_action == 1:  # Move left
            continuous_action = np.array([-0.1, 0], dtype=np.float32)

        if discrete_action == 2:  # Move up
            continuous_action = np.array([0, 0.1], dtype=np.float32)

        if discrete_action == 3:  # Move down
            continuous_action = np.array([0, -0.1], dtype=np.float32)
            
        return continuous_action



# The Network class inherits the torch.nn.Module class, which represents a neural network.
class Network(torch.nn.Module):

    # The class initialisation function. This takes as arguments the dimension of the network's input (i.e. the dimension of the state), and the dimension of the network's output (i.e. the dimension of the action).
    def __init__(self, input_dimension, output_dimension):
        # Call the initialisation function of the parent class.
        super(Network, self).__init__()
        # Define the network layers. This example network has two hidden layers, each with 100 units.
        self.layer_1 = torch.nn.Linear(in_features=input_dimension, out_features=100)
        self.layer_2 = torch.nn.Linear(in_features=100, out_features=100)
        self.output_layer = torch.nn.Linear(in_features=100, out_features=output_dimension)

    # Function which sends some input data through the network and returns the network's output. In this example, a ReLU activation function is used for both hidden layers, but the output layer has no activation function (it is just a linear layer).
    def forward(self, input):
        layer_1_output = torch.nn.functional.relu(self.layer_1(input))
        layer_2_output = torch.nn.functional.relu(self.layer_2(layer_1_output))
        output = self.output_layer(layer_2_output)
        return output


# The DQN class determines how to train the above neural network.
class DQN:

    # The class initialisation function.
    def __init__(self):
        # Create a Q-network, which predicts the q-value for a particular state.
        self.q_network = Network(input_dimension=2, output_dimension=4)
        self.target_network = Network(input_dimension=2, output_dimension=4)
        # Define the optimiser which is used when updating the Q-network. The learning rate determines how big each gradient step is during backpropagation.
        self.optimiser = torch.optim.Adam(self.q_network.parameters(), lr=0.001)
        self.discount = 0.9
    # Function that is called whenever we want to train the Q-network. Each call to this function takes in a transition tuple containing the data we use to update the Q-network.
    def train_q_network(self, transition):
        # Set all the gradients stored in the optimiser to zero.
        self.optimiser.zero_grad()
        # Calculate the loss for this transition.
        loss = self._calculate_loss(transition)
        # Compute the gradients based on this loss, i.e. the gradients of the loss with respect to the Q-network parameters.
        loss.backward()
        # Take one gradient step to update the Q-network.
        self.optimiser.step()
        # Return the loss as a scalar
        return loss.item()

    # Function to calculate the loss for a particular transition.
    def _calculate_loss(self, transition):
        # loss_sum = 0
        # for row in transition:
        #     action = row[1]
        #     reward = row[2]
        #     state = row[0]
        #     next_state = row[3]
        #     state_tensor = torch.tensor(state)
        #     next_state_tensor = torch.tensor(next_state).detach()
        #     network_prediction = self.q_network.forward(state_tensor)
        #     next_prediction = self.q_network.forward(next_state_tensor).detach()
        #     loss_sum += (reward + self.discount*max(next_prediction) - network_prediction[action])**2
        # return loss_sum/50


        states = []
        rewards = []
        actions = []
        next_states = []

        for row in transition:
            actions.append(row[1])
            rewards.append(row[2])
            states.append(row[0])
            next_states.append(row[3])

        states_tensor = torch.tensor(states)
        actions_tensor = torch.tensor(actions).unsqueeze(1)
        rewards_tensor = torch.tensor(rewards).unsqueeze(1)
        next_states_tensor = torch.tensor(next_states)


        #3.1 added
        next_state_network_predictions = self.target_network.forward(next_states_tensor).detach()
        max_next_states_tensor = []
        max_actions = []
        for row in next_state_network_predictions:
            max_actions.append(row.tolist().index(max(row.tolist())))
        max_actions_tensor = torch.tensor(max_actions).unsqueeze(1)
        
        next_predictions = torch.gather(next_state_network_predictions*0.9,1,max_actions_tensor)

        #stop here

        network_predictions = self.q_network.forward(states_tensor)

        predictions = torch.gather(network_predictions,1,actions_tensor)

        loss = torch.nn.MSELoss()(predictions,next_predictions+rewards_tensor)

        return loss

    def update_target(self):
        weights = self.q_network.state_dict()
        self.target_network.load_state_dict(weights)


class ReplayBuffer:

    def __init__(self):
        self.buffer = collections.deque()

class Visualisation:

    def __init__(self,magnification = 500):
        # Set the magnification factor of the display
        self.magnification = magnification
        self.width = 1.0
        self.height = 1.0
        self.image = np.zeros([int(self.magnification * self.height), int(self.magnification * self.width), 3], dtype=np.uint8)
    def visualise_Q(self,dqn):
        mag = self.magnification
        self.image.fill(0)

        for i in range(0,10):
            x_state = 0.05 + i*0.1
            for j in range(0,10):
                y_state = 0.95 - j*0.1
                states_tensor = torch.tensor([x_state,y_state])
                Q_value = dqn.q_network.forward(states_tensor)
                q_r = Q_value[0]
                q_l = Q_value[1]
                q_u = Q_value[2]
                q_d = Q_value[3]

                q_max = max(Q_value)
                q_min = min(Q_value)
                q_range = q_max - q_min
                #right
                cv2.fillConvexPoly(self.image,np.array([[26+int(mag/10*i),25+int(mag/10*j)],[49+int(mag/10*i),2+int(mag/10*j)],[49+int(mag/10*i),48+int(mag/10*j)]]),\
                    (int((1-(q_r-q_min)/q_range)*255),int(((q_r-q_min)/q_range)*255),int(((q_r-q_min)/q_range)*255)))
                #left
                cv2.fillConvexPoly(self.image,np.array([[24+int(mag/10*i),25+int(mag/10*j)],[1+int(mag/10*i),2+int(mag/10*j)],[1+int(mag/10*i),48+int(mag/10*j)]]),\
                    (int((1-(q_l-q_min)/q_range)*255),int(((q_l-q_min)/q_range)*255),int(((q_l-q_min)/q_range)*255)))
                #up
                cv2.fillConvexPoly(self.image,np.array([[25+int(mag/10*i),24+int(mag/10*j)],[2+int(mag/10*i),1+int(mag/10*j)],[48+int(mag/10*i),1+int(mag/10*j)]]),\
                    (int((1-(q_u-q_min)/q_range)*255),int(((q_u-q_min)/q_range)*255),int(((q_u-q_min)/q_range)*255)))
                #down
                cv2.fillConvexPoly(self.image,np.array([[25+int(mag/10*i),26+int(mag/10*j)],[2+int(mag/10*i),49+int(mag/10*j)],[48+int(mag/10*i),49+int(mag/10*j)]]),\
                    (int((1-(q_d-q_min)/q_range)*255),int(((q_d-q_min)/q_range)*255),int(((q_d-q_min)/q_range)*255)))
        
        for i in range(1,10):
            cv2.line(self.image,(int(i*mag/10),0), (int(i*mag/10),mag),(255,255,255),2)
            cv2.line(self.image,(0,int(i*mag/10)), (mag,int(i*mag/10)),(255,255,255),2)
        cv2.fillConvexPoly(self.image,np.array([[151,201],[151,349],[249,349],[249,201]]),\
                    (128,128,128))
        cv2.imshow("Q_value", self.image)
        cv2.waitKey(1)


    def visualise_P(self,transition):
        mag = self.magnification
        for i in range(1,10):
            cv2.line(self.image,(int(i*mag/10),0), (int(i*mag/10),mag),(255,255,255),2)
            cv2.line(self.image,(0,int(i*mag/10)), (mag,int(i*mag/10)),(255,255,255),2)

        for i in range(20):
            
            cur_state = transition[i][0]
            next_state = transition[i][3]
            cv2.line(self.image,(int(cur_state[0]*mag),int(mag-cur_state[1]*mag)), (int(next_state[0]*mag),int(mag-next_state[1]*mag)),(0,int(255*i/len(transition_list)),int(255-255*i/len(transition_list))),5)
        cv2.circle(self.image, (int(transition[0][0][0]*mag),int(mag-transition[0][0][1]*mag)), 7, [0,0,255], cv2.FILLED)
        cv2.circle(self.image, (int(transition[-1][0][0]*mag),int(mag-transition[-1][0][1]*mag)), 7, [0,255,0], cv2.FILLED)
        cv2.fillConvexPoly(self.image,np.array([[151,201],[151,349],[249,349],[249,201]]),\
                    (128,128,128))
        cv2.imshow("Greedy", self.image)
        cv2.waitKey(1)

# Main entry point
if __name__ == "__main__":
    # Set the random seed for both NumPy and Torch
    # You should leave this as 0, for consistency across different runs (Deep Reinforcement Learning is highly sensitive to different random seeds, so keeping this the same throughout will help you debug your code).
    CID = 796429
    np.random.seed(CID)
    torch.manual_seed(CID)
    delta = 0.004
    # Create an environment.
    # If display is True, then the environment will be displayed after every agent step. This can be set to False to speed up training time. The evaluation in part 2 of the coursework will be done based on the time with display=False.
    # Magnification determines how big the window will be when displaying the environment on your monitor. For desktop PCs, a value of 1000 should be about right. For laptops, a value of 500 should be about right. Note that this value does not affect the underlying state space or the learning, just the visualisation of the environment.
    environment = Environment(display=False, magnification=500)
    agent = None
    # Create an agent
    agent = Agent(environment,delta = delta)
    # Create a DQN (Deep Q-Network)
    dqn = DQN()
    count = 0
    loss_list = []
    replay = ReplayBuffer()
    # Loop over episodes
    while True:
        # Reset the environment for the start of the episode.
        agent.reset()
        # Loop over steps within this episode. The episode length here is 20.
        for step_num in range(20):
            # Step the agent once, and get the transition tuple for this step
            transition = agent.step(dqn)
            replay.buffer.append(transition)
            count += 1
            if count >= 50:
                indices = np.random.choice(range(0,count),50, replace = False)
                batch = []
                for i in indices:
                    batch.append(replay.buffer[i])
                loss = dqn.train_q_network(batch)
                loss_list.append(loss)
                agent.epsilon_decay()
        dqn.update_target()
        if count >= 500:
            break
    # plt.yscale("log")
    # plt.plot(loss_list)
    # plt.show()


    agent.reset()
    transition_list = []
    for step_num in range(20):
        transition = agent.greedy_step(dqn)
        transition_list.append(transition)


    visual = Visualisation()
    while True:
        visual.visualise_P(transition_list)
    #     #visual.visualise_Q(dqn)





